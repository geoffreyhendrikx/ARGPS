﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Vector Math (point - source).Normalize() 
/// </summary>
public class NewOrientation : MonoBehaviour
{
    public void RotateTowardsGameObject(GameObject go)
    {
        Vector3 pointTowards = (this.transform.position - go.transform.position).normalized;
        transform.eulerAngles = pointTowards;
    }
}
