﻿using ARLocation;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Vector Math (point - source).Normalize() 
/// </summary>
public class PointTowardsObject : MonoBehaviour
{
    private GameObject[] objects;
    [SerializeField]
    ARLocationManager locationManager;

    // Start is called before the first frame update
    private void Start()
    {
        var floorLevel = locationManager.CurrentGroundY;

        for (int i = 0; i < transform.childCount; i+=2)
        {
            if (transform.GetChild(i + 1) == null)
                break;

            objects = new GameObject[2]
            {
                transform.GetChild(i).GetChild(0).gameObject,
                transform.GetChild(i+1).GetChild(0).gameObject
            };

            Debug.Log(objects[0] + " <> " + objects[1]);
            var startPos = MathUtils.SetY(objects[0].transform.position, floorLevel);
            var endPos = MathUtils.SetY(objects[1].transform.position, floorLevel);
            var direction = (endPos - startPos).normalized;
            objects[0].transform.rotation = Quaternion.LookRotation(direction,Vector3.up);
            Debug.Log(direction);
            Debug.Break();
        }
    }
    [ContextMenu("UpdateAll")]
    public void UpdateAll()
    {
        Start();
    }

}
