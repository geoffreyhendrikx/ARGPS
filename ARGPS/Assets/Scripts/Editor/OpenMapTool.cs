﻿using UnityEditor;
using UnityEngine;

/// <summary>
/// Made because i don't have to search it everytime on google.
/// </summary>
public class OpenMapTool : Editor
{
    // Start is called before the first frame update
    [MenuItem("HS-Tools/Open WebMap")]
    private static void Init()
    {
        Application.OpenURL("https://editor.unity-ar-gps-location.com/");
    }
}
