﻿using UnityEditor;
using UnityEngine;

/// <summary>
/// Made because i don't have to search it everytime on google.
/// </summary>
public class OpenAPI : Editor
{
    // Start is called before the first frame update
    [MenuItem("HS-Tools/Open API")]
    private static void Init()
    {
        Application.OpenURL("https://docs.unity-ar-gps-location.com/");
    }
}
